# README #
This README would normally document whatever steps are necessary to get your application up and running.
WorldWilde Hotspot application

Imi propun ca aceasta aplicatie sa fie un travel hub online pentru cei care isi doresc sa impartaseasca experientele
lor legate de calatorii si locurile pe care le-au vizitat, dar si pentru a oferi utilizatorilor posibilitatea de a 
vedea imagini reale cu o locatie inainte de ajunge acolo.

Aplicatia va avea urmatoarele functionalitati:

1. LOGARE/INREGISTRARE SI FEEDBACK
Utilizatorul isi poate crea un cont cu ajutorul unui email/username si o parola.
Fara un cont, functionalitatea de istoric si de "impartaseste-ti povestea" nu vor putea fi utilizate.
De asemenea, utilizatorul poate acorda feedback pentru aplicatie si va avea o optiune prin care isi poate exprima
dorinta de a primi un raspuns pe email.
Un utlizator va putea sa adauce recenzii,like-uri, dislike-uri pe anumite povesti impartasite de alti useri.

2. DESCOPERA LUMEA
Aceasta va  fi functionalitatea de cautare si va fi utilizata cu mai multe optiuni:
-cautare dupa locatia utilizatorului(se va folosi geolocation API pentru a furniza datele necesare pentru Webcam
Travel API nearby, adica longitudine,latitudine etc)

-cautare dupa o tara introdusa

-cautare avansata, dupa anumite criterii, cum ar fi:
airport (Airport)
area (Area)
bay (Bay)
beach (Beach)
building (Building)
camping (Camping)
city (City)
coast (Coast)
forest (Forest)
golf (Golfcourse)
harbor (Harbor)
resort (Holiday Resort)
island (Island)
Acestea vor fi furnizate aceluiasi API prin category

-recomandari: Se va distribui lista cu top 10 cautari ale tuturor utilizatorilor

3. SHARE YOUR STORY
Utilizatorul va avea acces la lista completa de locatii si se va folosi de un search pentru a cauta locatia pentru
care isi doreste sa lase o poveste sau sa evalueze povestea altora.
Aceste povesti se vor sterge automat daca ating un anumit numar de dislike-uri

4. ISTORIC
Utilizatorul va avea acces la o lista cu ultimele sale cautari in aplicatie.
### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact